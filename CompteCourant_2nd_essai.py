from Compte_2nd_essai import Compte


class CompteCourant(Compte):

    """Docstring"""
    # attributes

    def __init__(self, nom: str, numero: str, solde: float, autorisation: float, agios: float):
        super().__init__(nom, numero, solde, str(type(self)).split("'")[1].split(".")[0])
        self.__autorisation_decouvert = autorisation
        self.__pourcentage_des_agios = agios

    def retrait(self, somme_a_retirer: float):
        self.set__solde(float(self.get__solde()) - somme_a_retirer)
        if float(self.get__solde()) < -self.__autorisation_decouvert:
            print(f"Le retrait sur votre compte numéroté {self.get__numero_compte()} "
                  f" dépasse le découvert autorisé. Des agios seront prélevés.")
            self.appliquer_agios()
        self.enregistrer_csv()

    def appliquer_agios(self):
        if self.get__solde() < - self.__autorisation_decouvert:
            self.set__solde(self.get__solde() * (1 + self.__pourcentage_des_agios))
        self.enregistrer_csv()
