from abc import ABC, abstractmethod


class Compte(ABC):

    """Docstring"""
    # attibutes

    @abstractmethod
    def __init__(self, nom: str, numero: str, solde: float, type_compte: str):
        self.__nom_du_proprietaire = nom
        self.__numero_du_compte = numero
        self.__solde = solde
        self.__type_du_compte = type_compte

    def retrait(self, somme_a_retirer: float):
        if somme_a_retirer > self.__solde:
            print(f"Le solde sur votre compte numéroté {self.__numero_du_compte} "
                  f"est insuffisant pour effectuer cette transaction.")
            print("Opération refusée.")
            return
        self.__solde -= somme_a_retirer
        self.enregistrer_csv()

    def versement(self, somme_a_ajouter: float):
        self.__solde += somme_a_ajouter
        self.enregistrer_csv()

    def afficher_solde(self):
        self.lire_csv()
        print(f"Madame, Monsieur {self.__nom_du_proprietaire},")
        if self.__solde >= 0:
            print(f"Votre compte numéroté {self.__numero_du_compte} est crédité de {self.__solde} euros.")
            return
        print(f"Votre compte numéroté {self.__numero_du_compte} est déficitaire de {- self.__solde} euros.")
        return

    def enregistrer_csv(self):
        try:
            with open(self.__type_du_compte + ".csv", 'w', encoding='utf-8') as compte_dir:
                compte_dir.write(
                    f"{self.__nom_du_proprietaire};{self.__numero_du_compte};{self.__solde}")
        except:
            print("Le fichier de sauvegarde ne peut pas être ouvert.")
            raise IOError
        print("Sauvegarde des données réussie.")

    def lire_csv(self):
        try:
            with open(self.__type_du_compte + ".csv", 'r', encoding='utf-8') as compte_dir:
                contenu = compte_dir.read()
                solde_actuel = contenu.split(";")[2]
                self.mise_a_jour_du_solde(float(solde_actuel))
        except:
            print("Le fichier de sauvegarde ne peut pas être ouvert.")
            raise IOError
        print("Lecture des données réussie.")

    def mise_a_jour_du_solde(self, nouveau_montant: float):
        self.__solde = nouveau_montant

    def get__solde(self) -> float:
        return self.__solde

    def set__solde(self, nouveau_solde: float):
        self.__solde = nouveau_solde

    def get__numero_compte(self) -> str:
        return self.__numero_du_compte
