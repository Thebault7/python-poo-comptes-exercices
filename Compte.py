from abc import ABC, abstractmethod


class Compte(ABC):

    """Docstring"""
    # attibutes

    @abstractmethod
    def __init__(self, nom: str, numero: int, solde: float):
        self.__nom_du_proprietaire = nom
        self.__numero_du_compte = numero
        self.__solde = solde

    def retrait(self, somme_a_retirer: float, autorisation) -> bool:
        if self.__solde + somme_a_retirer < autorisation:
            self.__solde += somme_a_retirer
            return True
        else:
            self.__solde += somme_a_retirer
            return False

    def versement(self, somme_a_ajouter: float) -> float:
        pass

    def afficher_solde(self):
        print(self.__solde)

    def get_nom_du_proprietaire(self) -> str:
        return self.__nom_du_proprietaire

    def set_nom_du_proprietaire(self, nom: str):
        self.__nom_du_proprietaire = nom

    def get_numero_du_compte(self) -> int:
        return self.__numero_du_compte

    def set_numero_du_compte(self, numero: int):
        self.__numero_du_compte = numero

    def get_solde(self) -> float:
        return self.__solde

    def set_solde(self, solde: float):
        self.__solde = solde


