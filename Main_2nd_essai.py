from CompteCourant_2nd_essai import CompteCourant
from CompteEpargne_2nd_essai import CompteEpargne


def demarrer_app():
    print("Bienvenu sur le site de votre banque.")
    nom = input("Veuillez entrer votre nom : ")
    compte_epargne = CompteEpargne(nom, "254374535", 3452.7, 0.05)
    compte_courant = CompteCourant(nom, "758436244", 743, 200, 0.2)
    compte_epargne.enregistrer_csv()
    compte_courant.enregistrer_csv()

    print(f"Bienvenu(e) Madame/Monsieur {nom}.")
    continuer = True
    while continuer:
        liste = {"c", "C", "e", "E", "q", "Q"}
        quel_compte = ""
        while not liste.__contains__(quel_compte):
            print("Quel compte souhaitez-vous utiliser ?")
            print("Tapez C puis ENTRER pour le compte courant.")
            print("Tapez E puis ENTRER pour le compte épargne.")
            quel_compte = input("Tapez Q puis ENTRER pour quitter.")
            if not liste.__contains__(quel_compte):
                print("Erreur, lettre non reconnue.")

        if quel_compte == "q" or quel_compte == "Q":
            print("Arrêt de l'application. Bonne journée.")
            break

        if quel_compte == "e" or quel_compte == "E":
            compte_en_usage = compte_epargne

        if quel_compte == "c" or quel_compte == "C":
            compte_en_usage = compte_courant

        compte_en_usage.lire_csv()
        compte_en_usage.afficher_solde()

        liste_action = {"a", "A", "r", "R"}
        action = ""
        while not liste_action.__contains__(action):
            print("Souhaitez-vous ajouter de l'argent ? Appuyez sur A puis sur ENTRER.")
            action = input("Souhaitez-vous retirer de l'argent ? Appuyez-sur R puis sur ENTRER.")
            if not liste_action.__contains__(action):
                print("Erreur, veuillez entrer l'action souhaitée.")
        if action == "a" or action == "A":
            somme_a_ajouter = float(input("Quelle somme souhaitez-vous ajouter ?"))
            compte_en_usage.versement(somme_a_ajouter)
        if action == "r" or action == "R":
            somme_a_retirer = float(input("Quelle somme souhaitez-vous retirer ?"))
            compte_en_usage.retrait(somme_a_retirer)

        compte_en_usage.afficher_solde()

        print("Souhaitez vous continuer par une autre opération ?")
        print("Si oui, appuyer sur n'importe quelle touche avant d'appuyer sur ENTRER.")
        continuer = input("Si non, appuyer simplement sur ENTRER.")


demarrer_app()
