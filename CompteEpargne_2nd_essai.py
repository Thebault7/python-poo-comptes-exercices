from Compte_2nd_essai import Compte


class CompteEpargne(Compte):
    """Docstring"""

    # attributes

    def __init__(self, nom: str, numero: str, solde: float, pourcentage: float):
        super().__init__(nom, numero, solde, str(type(self)).split("'")[1].split(".")[0])
        self.__pourcentage_interets = pourcentage

    def appliquer_interets(self):
        self.set__solde(self.get__solde() * (1 + self.__pourcentage_interets))
        self.enregistrer_csv()
