import Compte


class CompteCourant(Compte.Compte):

    """Docstring"""
    # attributes

    def __init__(self, nom: str, numero: int, solde: float, autorisation: float, agio: float):
        super().__init__(nom, numero, solde)
        super().set_nom_du_proprietaire(nom)
        super().set_numero_du_compte(numero)
        super().set_solde(solde)
        self.__autorisation_de_decouvert = autorisation
        self.__pourcentage_agios = agio

    def retrait(self, somme_a_retirer: float):
        appliquer_agios = super().retrait(somme_a_retirer, self.__autorisation_de_decouvert)
        if appliquer_agios:
            self.appliquer_agios()

    def versement(self, somme_a_ajouter: float):
        super().set_solde(super().get_solde() + somme_a_ajouter)

    def afficher_solde(self):
        super().afficher_solde()

    def appliquer_agios(self):
        super().set_solde(super().get_solde() * (1 + self.__pourcentage_agios))



