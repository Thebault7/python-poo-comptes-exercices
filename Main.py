import CompteCourant


def demarrer_app():
    solde = float(input("Quel est le solde actuel ?"))
    nom = input("Quel est votre nom ?")
    numero = str(input("Quel est le numéro de compte ?"))
    autorisation = float(input("Quel est l'autorisation de découvert ?"))
    agio = float(input("Quel est le pourcentage des agios ?"))

    compte_du_client = CompteCourant.CompteCourant(nom, numero, solde, autorisation, agio)

    continuer = True
    while continuer:
        compte_du_client.afficher_solde()
        #compte = input("Sur quel compte souhaitez-vous opérer ?")
        transaction = float(input("Quelle est le montant de la transaction ?"))

        if transaction < 0:
            compte_du_client.retrait(transaction)
            compte_du_client.afficher_solde()
        else:
            compte_du_client.versement(transaction)
            compte_du_client.afficher_solde()

        continuer = input("Taper simplement ENTRER pour quitter, tout autre caractère pour continuer.")


demarrer_app()
